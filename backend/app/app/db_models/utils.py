from base64 import b64encode


def track_base64_from_uri_n_location(context) -> bytes:
    """Parse table data and returns a base64 id for entry."""
    if context.current_parameters["llresponse"]["loadType"] in [
        "PLAYLIST_LOADED",
        "SEARCH_RESULT",
    ]:
        uri = context.current_parameters["query"]
    else:
        uri = "".join(
            [t["info"]["uri"] for t in context.current_parameters["llresponse"]["tracks"]]
        )
    return b64encode(uri.encode("utf-8"))


