import datetime

from sqlalchemy import Column, String, LargeBinary, DateTime
from sqlalchemy.dialects.postgresql import JSONB

from app.db.base_class import Base
from app.db_models.utils import track_base64_from_uri_n_location


class Query(Base):
    id = Column(
        LargeBinary, primary_key=True, index=True, default=track_base64_from_uri_n_location
    )
    author = Column(String, index=True)
    title = Column(String, index=True)
    request = Column(String, index=True, unique=True)
    llresponse = Column(JSONB, default={})
    uri = Column(String, index=True)
    updated_on = Column(
        DateTime(timezone=True), default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow
    )

