import datetime

from sqlalchemy import Boolean, Column, Integer, DateTime, String, BIGINT

from app.db.base_class import Base


class User(Base):
    id = Column(Integer, primary_key=True, autoincrement=True)
    user_id = Column(BIGINT, unique=True, nullable=True)
    entries_submitted = Column(BIGINT, default=0)

    is_guest = Column(Boolean(), default=True)
    is_user = Column(Boolean(), default=False)
    is_contributor = Column(Boolean(), default=False)
    is_mod = Column(Boolean(), default=False)
    is_admin = Column(Boolean(), default=False)
    is_superuser = Column(Boolean(), default=False)
    is_blacklisted = Column(Boolean(), default=False)

    registered_on = Column(DateTime(timezone=True), default=datetime.datetime.utcnow)
    updated_on = Column(DateTime(timezone=True), default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)
    blacklisted_on = Column(DateTime(timezone=True), default=None, onupdate=datetime.datetime.utcnow, nullable=True)

    token = Column(String, index=True, nullable=True)
    md5 = Column(String, index=True, unique=True, nullable=False)
