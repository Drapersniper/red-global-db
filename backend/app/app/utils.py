import asyncio
import hashlib
import uuid
from typing import Union, Any, AsyncIterator, Sequence


def uuid_from_id(seed: Union[str, int]) -> str:
    m = hashlib.md5()
    m.update(f"{seed}".encode('utf-8'))
    return f"{uuid.UUID(m.hexdigest())}"


async def async_iterate(sequence: Sequence, sleep: float = 0.0001) -> AsyncIterator[Any]:
    for i in sequence:
        yield i
        await asyncio.sleep(sleep)


def has_hierarchy(current_user, user_in) -> bool:
    has_points = 0
    if user_in.is_superuser:
        return False

    if current_user.is_superuser:
        has_points += 5
    elif current_user.is_admin:
        has_points += 4
    elif current_user.is_mod:
        has_points += 3
    if has_points == 0:
        return False
    need_points = 0
    if current_user.is_admin:
        need_points += 5
    elif current_user.is_mod:
        need_points += 4
    elif current_user.is_contributor:
        need_points += 3
    elif current_user.is_user:
        need_points += 2

    return current_user > need_points
