from typing import List, Optional

from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session

from app.db_models.query import Query
from app.models.query import QueryCreate, QueryUpdate


def get(db_session: Session, *, id: int) -> Optional[Query]:
    return db_session.query(Query).filter(Query.id == id).first()


def get_by_athor(db_session: Session, *, author: str, ilike=False) -> Optional[Query]:
    filters = []
    if ilike and author:
        filters.append(Query.request.ilike(f'%{author}%'))
    elif author:
        filters.append(Query.request == author)
    else:
        return None
    return db_session.query(Query).filter(*filters).order_by(Query.updated_on.desc()).first()


def get_by_uri(db_session: Session, *, uri: str, ilike=False) -> Optional[Query]:
    filters = []
    if ilike and uri:
        filters.append(Query.uri.ilike(f'%{uri}%'))
    elif uri:
        filters.append(Query.request == uri)
    else:
        return None
    return db_session.query(Query).filter(*filters).order_by(Query.updated_on.desc()).first()


def get_by_request(db_session: Session, *, query: str, ilike=False) -> Optional[Query]:
    filters = []
    if ilike and query:
        filters.append(Query.request.ilike(f'%{query}%'))
    elif query:
        filters.append(Query.request == query)
    else:
        return None
    return db_session.query(Query).filter(*filters).order_by(Query.updated_on.desc()).first()


def get_by_title(db_session: Session, *, title: str, ilike=False) -> Optional[Query]:
    filters = []
    if ilike and title:
        filters.append(Query.request.ilike(f'%{title}%'))
    elif title:
        filters.append(Query.request == title)
    else:
        return None
    return db_session.query(Query).filter(*filters).order_by(Query.updated_on.desc()).first()


def get_multi(db_session: Session, *, skip=0, limit=100) -> List[Optional[Query]]:
    return db_session.query(Query).offset(skip).limit(limit).all()

def get_multi_by_author(
    db_session: Session, *, author: str, ilike=False, skip=0, limit=100
) -> List[Optional[Query]]:
    filters = []
    if ilike and author:
        filters.append(Query.request.ilike(f'%{author}%'))
    elif author:
        filters.append(Query.request == author)
    else:
        return []
    return (
        db_session.query(Query)
        .filter(*filters)
        .order_by(Query.updated_on.desc())
        .offset(skip)
        .limit(limit)
        .all()
    )

def get_multi_by_request(
    db_session: Session, *, query: str, ilike=False, skip=0, limit=100
) -> List[Optional[Query]]:
    filters = []
    if ilike and query:
        filters.append(Query.request.ilike(f'%{query}%'))
    elif query:
        filters.append(Query.request == query)
    else:
        return []

    return (
        db_session.query(Query)
        .filter(*filters)
        .order_by(Query.updated_on.desc())
        .offset(skip)
        .limit(limit)
        .all()
    )
def get_multi_by_uri(
    db_session: Session, *, uri: str, ilike=False, skip=0, limit=100
) -> List[Optional[Query]]:
    filters = []
    if ilike and uri:
        filters.append(Query.request.ilike(f'%{uri}%'))
    elif uri:
        filters.append(Query.request == uri)
    else:
        return []

    return (
        db_session.query(Query)
        .filter(*filters)
        .order_by(Query.updated_on.desc())
        .offset(skip)
        .limit(limit)
        .all()
    )


def get_multi_by(
    db_session: Session, *, title: str=None, query: str=None, author: str=None, uri=None, ilike=False, skip=0, limit=100
) -> List[Optional[Query]]:
    filters = []

    if all(not x for x in [title, query, author, uri]):
        return []
    if ilike:
        if title:
            filters.append(Query.title.ilike(f'%{query}%'))
        if author:
            filters.append(Query.author.ilike(f'%{query}%'))
        if query:
            filters.append(Query.request.ilike(f'%{query}%'))
        if uri:
            filters.append(Query.uri.ilike(f'%{uri}%'))
    else:
        if title:
            filters.append(Query.title == title)
        if author:
            filters.append(Query.author == author)
        if query:
            filters.append(Query.request == query)
        if uri:
            filters.append(Query.uri == uri)
    return (
        db_session.query(Query)
        .filter(*filters)
        .order_by(Query.updated_on.desc())
        .offset(skip)
        .limit(limit)
        .all()
    )

def get_by(
    db_session: Session, *, title: str=None, query: str=None, author: str=None, uri=None, ilike=False
) -> Optional[Query]:
    filters = []
    if all(not x for x in [title, query, author, uri]):
        return None
    if ilike:
        if title:
            filters.append(Query.title.ilike(f'%{query}%'))
        if author:
            filters.append(Query.author.ilike(f'%{query}%'))
        if query:
            filters.append(Query.request.ilike(f'%{query}%'))
        if uri:
            filters.append(Query.uri.ilike(f'%{uri}%'))
    else:
        if title:
            filters.append(Query.title == title)
        if author:
            filters.append(Query.author == author)
        if query:
            filters.append(Query.request == query)
        if uri:
            filters.append(Query.uri == uri)

    return (
        db_session.query(Query)
        .filter(*filters)
        .first()
    )

def create(db_session: Session, *, item_in: QueryCreate) -> Query:
    item_in_data = jsonable_encoder(item_in)
    item = Query(**item_in_data)
    db_session.add(item)
    db_session.commit()
    db_session.refresh(item)
    return item


def update(db_session: Session, *, item: Query, item_in: QueryUpdate) -> Query:
    item_data = jsonable_encoder(item)
    update_data = item_in.dict(skip_defaults=True)
    for field in item_data:
        if field in update_data:
            setattr(item, field, update_data[field])
    db_session.add(item)
    db_session.commit()
    db_session.refresh(item)
    return item


def remove(db_session: Session, *, query: str):
    item = db_session.query(Query).filter(Query.request == query).first()
    db_session.delete(item)
    db_session.commit()
    return item
