from secrets import token_urlsafe
from typing import List, Optional, Union

from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session

from app.db_models.user import User
from app.models.user import UserCreate, UserUpdate, UserInserts


def get_by_id(db_session: Session, *, user_id: int) -> Optional[User]:
    return db_session.query(User).filter(User.user_id == user_id).first()


def get_by_token(db_session: Session, *, token: str) -> Optional[User]:
    return db_session.query(User).filter(User.token == token).first()


def get_by_md5(db_session: Session, *, md5: str) -> Optional[User]:
    return db_session.query(User).filter(User.md5 == md5).first()


def authenticate(db_session: Session, *, token: str = None, md5: str = None) -> Optional[User]:
    if token:
        user = get_by_token(db_session, token=token)
    elif md5:
        user = get_by_md5(db_session, md5=md5)
    else:
        user = None
    if not user and md5:
        user = create(db_session=db_session, user_in=UserCreate(md5=md5))
    elif not user:
        return None
    if user.is_blacklisted:
        return None
    return user


def is_blacklisted(user) -> bool:
    return user.is_blacklisted


def is_superuser(user) -> bool:
    return user.is_superuser


def is_admin(user) -> bool:
    return user.is_admin


def is_mod(user) -> bool:
    return user.is_mod


def is_contributor(user) -> bool:
    return user.is_contributor


def get_multi(db_session: Session, *, skip=0, limit=100) -> List[Optional[User]]:
    return db_session.query(User).offset(skip).order_by(User.entries_submitted.desc()).limit(limit).all()


def get_multi_blacklisted(db_session: Session, *, skip=0, limit=100) -> List[Optional[User]]:
    return db_session.query(User).filter(User.is_blacklisted is True).offset(skip).limit(limit).all()


def create(db_session: Session, *, user_in: UserCreate) -> User:
    user = User(
        md5=user_in.md5,
        token=token_urlsafe(128) if user_in.is_superuser else token_urlsafe(64) if user_in.is_admin else token_urlsafe(48) if user_in.is_mod else token_urlsafe(32) if user_in.is_contributor else None,
        user_id=user_in.user_id,
        is_superuser=user_in.is_superuser,
        is_admin=user_in.is_admin,
        is_mod=user_in.is_mod,
        is_contributor=user_in.is_contributor,
        is_user=user_in.is_user,
        is_guest=user_in.is_guest,
        is_blacklisted=user_in.is_blacklisted
    )
    db_session.add(user)
    db_session.commit()
    db_session.refresh(user)
    return user


def update(db_session: Session, *, user: User, user_in: Union[UserUpdate, UserInserts], revoke_token: bool = False, blacklist: bool = False) -> User:
    user_data = jsonable_encoder(user)
    update_data = user_in.dict(skip_defaults=True)
    for field in user_data:
        if field in ["md5", "user_id", "entries_submitted", "is_superuser", "user_id"]:
            continue
        if field in update_data:
            if revoke_token and field == "token":
                update_data[field] = None
            elif blacklist and field == "token":
                update_data[field] = None
            setattr(user, field, update_data[field])

    db_session.add(user)
    db_session.commit()
    db_session.refresh(user)
    return user
