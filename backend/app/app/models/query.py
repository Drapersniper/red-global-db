from collections import MutableMapping
from enum import Enum
from typing import Union, List, Optional

from pydantic import BaseModel

from app.api.utils.helper_classes import ExceptionSeverity


class LavalinkLoadType(str, Enum):
    TRACK_LOADED = "TRACK_LOADED"
    PLAYLIST_LOADED = "PLAYLIST_LOADED"
    SEARCH_RESULT = "SEARCH_RESULT"
    V2_COMPAT = "V2_COMPAT"


class LavalinkLoadTypeFull(str, Enum):
    TRACK_LOADED = "TRACK_LOADED"
    PLAYLIST_LOADED = "PLAYLIST_LOADED"
    SEARCH_RESULT = "SEARCH_RESULT"
    V2_COMPAT = "V2_COMPAT"
    NO_MATCHES = "NO_MATCHES"
    LOAD_FAILED = "LOAD_FAILED"


class LavalinkExceptions(BaseModel):
    message: str
    severity: ExceptionSeverity

    def jsonfy(self) -> MutableMapping:
        return self.dict(skip_defaults=False)


class BaseResponseModel(BaseModel):
    details: str


class LavalinkPlaylist(BaseModel):
    name: Optional[str]
    selectedTrack: int = 0

    def jsonfy(self) -> MutableMapping:
        return self.dict(skip_defaults=False)


class LavalinkTrackInfo(BaseModel):
    identifier: str
    isSeekable: bool = False
    author: str
    length: int = 0
    isStream: bool = False
    position: int
    title: str
    uri: str
    timestamp: int = 0
    extras: dict = {}

    def jsonfy(self) -> MutableMapping:
        return self.dict(skip_defaults=False)


class LavalinkTrack(BaseModel):
    track: str
    info: LavalinkTrackInfo

    def jsonfy(self) -> MutableMapping:
        return self.dict(skip_defaults=False)


class LavalinkResponse(BaseModel):
    loadType: LavalinkLoadType
    playlistInfo: Union[LavalinkPlaylist, dict] = {}
    tracks: List[LavalinkTrack]

    def jsonfy(self) -> MutableMapping:
        return self.dict(skip_defaults=False)


class LavalinkQueryResponse(BaseModel):
    loadType: Optional[LavalinkLoadTypeFull]
    playlistInfo: Union[LavalinkPlaylist, dict, None] = {}
    tracks: List[LavalinkTrack] = []

    def jsonfy(self) -> MutableMapping:
        return dict(self)


# Shared properties
class QueryBase(BaseModel):
    request: str = None
    llresponse: dict = None


# Properties to receive on item creation
class QueryCreate(QueryBase):
    request: str
    author: str
    title: str
    uri: str
    llresponse: dict


# Properties to receive on item update
class QueryUpdate(QueryBase):
    request: str
    uri: str
    llresponse: dict
    author: str = None
    title: str = None


# Properties shared by models stored in DB
class QueryInDBBase(QueryBase):
    request: str
    author: str
    title: str
    llresponse: dict


# Properties to return to client
class Query(QueryInDBBase):
    pass


# Properties properties stored in DB
class QueryInDB(QueryInDBBase):
    pass
