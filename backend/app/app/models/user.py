from typing import Optional

from pydantic import BaseModel


class UserInserts(BaseModel):
    entries_submitted: int


# Shared properties
class UserBase(BaseModel):
    md5: Optional[str] = None
    is_blacklisted: Optional[bool] = True
    is_superuser: Optional[bool] = False
    is_admin: Optional[bool] = None
    is_mod: Optional[bool] = None
    is_contributor: Optional[bool] = None
    is_user: Optional[bool] = None
    is_guest: Optional[bool] = None


class UserBaseInDB(UserBase):
    md5: str


# Properties to receive via API on creation
class UserCreate(UserBase):
    user_id: int
    is_blacklisted: bool = False
    is_superuser: bool = False
    is_admin: bool = False
    is_mod: bool = False
    is_contributor: bool = False
    is_user: bool = False
    is_guest: bool = True
    token: Optional[str] = ""


# Properties to receive via API on update
class UserUpdate(UserBaseInDB):
    is_blacklisted: bool = False
    is_superuser: bool = False
    is_admin: bool = False
    is_mod: bool = False
    is_contributor: bool = False
    is_user: bool = False
    is_guest: bool = True

    entries_submitted: Optional[int] = 0
    user_id: Optional[int] = None
    token: Optional[str] = ""


# Additional properties to return via API
class User(UserBaseInDB):
    pass


# Additional properties stored in DB
class UserInDB(UserBaseInDB):
    entries_submitted: int
    is_blacklisted: bool
    is_superuser: bool
    is_admin: bool
    is_mod: bool
    is_contributor: bool
    is_user: bool
    is_guest: bool
    user_id: Optional[int] = None
    token: Optional[str] = None
