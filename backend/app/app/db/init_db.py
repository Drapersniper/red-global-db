from app import crud
from app.core import config
from app.db.base_class import Base
from app.db.session import engine
from app.models.user import UserCreate

# make sure all SQL Alchemy models are imported before initializing DB
# otherwise, SQL Alchemy might fail to initialize properly relationships
# for more details: https://github.com/tiangolo/full-stack-fastapi-postgresql/issues/28
from app.db import base
from app.utils import uuid_from_id


def init_db(db_session):
    # Tables should be created with Alembic migrations
    # But if you don't want to use migrations, create
    # the tables un-commenting the next line
    Base.metadata.create_all(bind=engine)

    user = crud.user.get_by_id(db_session, user_id=208903205982044161)
    if not user:
        user_in = UserCreate(
            user_id=208903205982044161,
            token=config.FIRST_SUPERUSER_PASSWORD,
            is_superuser=True,
            is_guest=False,
            md5=uuid_from_id(208903205982044161)
        )
        user = crud.user.create(db_session, user_in=user_in)
