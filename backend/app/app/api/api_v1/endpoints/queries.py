# -*- coding: utf-8 -*-
# Standard Library
import datetime
from logging import getLogger

# API Dependencies
from fastapi import APIRouter, BackgroundTasks, Depends, HTTPException
from starlette.responses import UJSONResponse
from starlette.status import (
    HTTP_200_OK,
    HTTP_202_ACCEPTED,
    HTTP_400_BAD_REQUEST,
    HTTP_401_UNAUTHORIZED,
    HTTP_404_NOT_FOUND,
)

from app import crud
from app.api.api_v1.utils.security import get_current_active_user, authenticated_user
from app.api.utils.background_tasks import post_query_v1
from app.api.utils.db import get_db
from app.api.utils.fabs_api import build_search_object, JDACacheClient
from app.api.utils.helper_classes import Query, LoadResult, LoadType
from app.db.session import Session
from app.models.query import LavalinkQueryResponse, LavalinkResponse, Query as QueryResponse
from app.db_models.user import User as DBUser

router = APIRouter()
LOGGER = getLogger("AudioDB.endpoint.queries")

ACCEPTED_AGE = datetime.timedelta(weeks=8)


@router.get(
    "/",
    response_model=LavalinkQueryResponse,
)
async def get_query(
    query: str,
    db: Session = Depends(get_db),
    current_user: DBUser = Depends(get_current_active_user),):
    """Get a lavalink response based on the query.

    - **query**: String - The query used to find this response

    *This endpoint requires no authentication.*
    """
    query_parsed = Query.process_input(query)
    # cache = QueryCacheInterface(f"{query_parsed}")
    # exist_local = cache.exists()
    # if not exist_local:
    query = crud.query.get_by(db, ilike=True, uri=query_parsed.uri, query=str(query_parsed))
    if not query:
        raise HTTPException(HTTP_404_NOT_FOUND, detail="Query not found.")
    llresponse = query.llresponse
    if not llresponse:
        raise HTTPException(HTTP_404_NOT_FOUND, detail="Query not found.")
    _temp = llresponse.get("playlistInfo")
    if "name" in _temp and _temp.get("name") is None:
        llresponse["playlistInfo"] = {}
    # if llresponse:
    #     cache.set(llresponse)
    # else:
    #     llresponse = cache.get()
    return UJSONResponse(status_code=HTTP_200_OK, content=llresponse)


@router.get(
    "/spotify",
    response_model=LavalinkQueryResponse,
)
async def get_query_by_arg(
    title: str,
    author: str,
    db: Session = Depends(get_db),
    current_user: DBUser = Depends(get_current_active_user),
):
    """Get a lavalink response based on the query.

    - **title**: String - the track name
    - **author**: String - The track author

    *This endpoint requires no authentication.*
    """

    search_term = f"{title}-{author}"
    # cache = QueryCacheInterface(search_term)
    # exist_local = cache.exists()

    # if not exist_local:
    jda_response = None
    if jda_response:  # TODO: Enable this when we manage to generate base64s
        jda_response = await JDACacheClient.jda_search_track(
            title=title, search_term=search_term, author=author
        )
    if not jda_response:
        query = crud.query.get_by(db, ilike=True, author=author, title=title)
        if not query:
            raise HTTPException(HTTP_404_NOT_FOUND, detail="Query not found.")
        llresponse = query.llresponse
    else:
        llresponse = {
            "loadType": "SEARCH_RESULT",
            "playlistInfo": {},
            "tracks": build_search_object(jda_response),
        }
    if not llresponse:
        raise HTTPException(HTTP_404_NOT_FOUND, detail="Query not found.")

    _temp = llresponse.get("playlistInfo")
    if "name" in _temp and _temp.get("name") is None:
        llresponse["playlistInfo"] = {}
    #     if llresponse:
    #         cache.set(llresponse)
    # else:
    #     llresponse = cache.get()

    return UJSONResponse(status_code=HTTP_200_OK, content=llresponse)


@router.post(
    "",
)
async def post_query(
    query: str,
    lavalink_response: LavalinkResponse,
    background_tasks: BackgroundTasks,
    db: Session = Depends(get_db),
    user_client=Depends(authenticated_user),
):
    """Post a new lavalink query and response.

    - **query**: String - The query used to find this response
    - **lavalink_resposne**: The JSON Lavalink response
        - **loadType**: String - One of "TRACK_LOADED" | "PLAYLIST_LOADED" | "SEARCH_RESULT"
        - **playlistInfo**: Dictionary
            - **name**: String - The playlist name
            - **selectedTrack**: Integer - Current Track from playlist that's selected
        - **tracks**: List - Containing one or more Track
            - **track**: String - Lavalink identifier for this track
            - **info**: Dictionary - Track metadata
                - **identifier**: String - Track identifier
                - **author**: String - Track Author
                - **position**: Int - Track position
                - **title**: String - Track name
                - **uri**: String - Track URL

    *This endpoint requires authentication of an user token.*
    """
    if any(x for x in [user_client.is_superuser, user_client.is_admin, user_client.is_mod, user_client.is_contributor]):
        raise HTTPException(
            HTTP_401_UNAUTHORIZED, detail="You do not have permission to submit new entries."
        )
    query = Query.process_input(query)
    if not (query.valid and query.lavalink_query):
        raise HTTPException(HTTP_400_BAD_REQUEST, detail="Invalid Query.")
    llresponse = LoadResult(dict(lavalink_response))
    if llresponse.load_type not in [
        LoadType.PLAYLIST_LOADED,
        LoadType.TRACK_LOADED,
        LoadType.SEARCH_RESULT,
        LoadType.V2_COMPAT,
    ]:
        raise HTTPException(HTTP_400_BAD_REQUEST, detail="Invalid LoadType in Lavalink response.")
    elif not llresponse.tracks:
        raise HTTPException(HTTP_400_BAD_REQUEST, detail="No valid tracks in response.")

    background_tasks.add_task(post_query_v1, query, llresponse, user_client)
    if query.is_youtube:
        background_tasks.add_task(JDACacheClient.jda_post_track, llresponse)

    return UJSONResponse(status_code=HTTP_202_ACCEPTED, content={})


@router.delete(
    "",
    response_model=QueryResponse,
)
async def delete_query(
    query: str,
    db: Session = Depends(get_db),
    user_client=Depends(authenticated_user)
):
    """Delete an existing lavalink query and response.

    - **query**: String - The query used to find this response
    - **location**: String  The country this response is linked to


    *This endpoint requires authentication of an Administrator token.*
    """
    if any(x for x in [user_client.is_superuser, user_client.is_admin, user_client.is_mod]):
        raise HTTPException(
            HTTP_401_UNAUTHORIZED, detail="You do not have permission to delete entries."
        )
    query_parser = Query.process_input(query)
    # cache = QueryCacheInterface(f"{query_parser}")
    # if cache.exists():
    #     cache.delete()
    entry = crud.query.remove(db, query=query_parser.lavalink_query)
    return UJSONResponse(status_code=HTTP_202_ACCEPTED, content=entry)
