from typing import List

from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session

from app import crud
from app.api.utils.db import get_db
from app.api.utils.security import authenticated_user
from app.db_models.user import User as DBUser
from app.models.user import User, UserCreate, UserInDB, UserUpdate
from app.utils import has_hierarchy

router = APIRouter()


@router.get("/", response_model=List[User])
def read_users(
    db: Session = Depends(get_db),
    skip: int = 0,
    limit: int = 100,
    current_user: DBUser = Depends(authenticated_user),
):
    """
    Retrieve users.
    """
    users = crud.user.get_multi(db, skip=skip, limit=limit)
    filtered = []
    for user in users:
        if has_hierarchy(current_user, user):
            filtered.append(user)
    return users


@router.post("/", response_model=User)
def create_user(
    *,
    db: Session = Depends(get_db),
    user_in: UserCreate,
    current_user: DBUser = Depends(authenticated_user),
):
    """
    Create new user.
    """
    if not has_hierarchy(current_user, user_in):
        raise HTTPException(
            status_code=400, detail="The user doesn't have enough privileges"
        )
    user = crud.user.get_by_id(db, user_id=UserCreate.user_id)
    if user:
        raise HTTPException(
            status_code=400,
            detail="The user with this username already exists in the system.",
        )
    user = crud.user.create(db, user_in=user_in)
    return user


@router.get("/{user_id}", response_model=User)
def read_user_by_id(
    user_id: int,
    current_user: DBUser = Depends(authenticated_user),
    db: Session = Depends(get_db),
):
    """
    Get a specific user by id.
    """
    user = crud.user.get_by_id(db, user_id=user_id)
    if user == current_user:
        return user
    if not has_hierarchy(current_user, user):
        raise HTTPException(
            status_code=400, detail="The user doesn't have enough privileges"
        )
    return user


@router.put("/{user_id}", response_model=User)
def update_user(
    *,
    db: Session = Depends(get_db),
    user_id: int,
    user_in: UserUpdate,
    revoke_token:bool = False,
    blacklist:bool = False,
    current_user: UserInDB = Depends(authenticated_user),
):
    """
    Update a user.
    """
    user = crud.user.get_by_id(db, user_id=user_id)
    if user == current_user:
        return user
    if not has_hierarchy(current_user, user):
        raise HTTPException(
            status_code=400, detail="The user doesn't have enough privileges"
        )
    user = crud.user.update(db, user=user, user_in=user_in, revoke_token=revoke_token, blacklist=blacklist)
    return user
