from fastapi import APIRouter

from app.api.api_v1.endpoints import users, queries

api_router = APIRouter()
api_router.include_router(queries.router, prefix="/queries", tags=["queries"])
api_router.include_router(users.router, prefix="/users", tags=["users"])
