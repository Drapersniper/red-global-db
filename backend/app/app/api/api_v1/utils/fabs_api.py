# -*- coding: utf-8 -*-
# Standard Library
import asyncio
import base64
import contextlib
import os
import struct
import sys

from logging import getLogger
from typing import List, Mapping, NoReturn, Optional, MutableMapping

# API Dependencies
import aiohttp

# API Imports
from app.api.utils.helper_classes import LoadResult, LoadType
from app.api.utils.json import loads as JSON_DECODER

LOGGER = getLogger("AudioDB.utils.JDAAPI")

POST_SEARCH_ENDPOINT = "https://cache.notfab.net/track/search"
POST_TRACK = "https://cache.notfab.net/track/index"


class _JDACacheClient:
    def __init__(self) -> None:
        self.session = aiohttp.ClientSession()
        self.headers = {
            "X-Index": "Youtube",
            "Authorization": self._get_api_key(),
            "X-API": "2.0",
            "Content-Type": "application/json",
        }

    @staticmethod
    def _get_api_key() -> Optional[str]:
        return os.getenv("JDAAPI")

    @staticmethod
    def track_generator(track: Mapping) -> MutableMapping:
        track_mapped = {"track": None, "info": {}}
        for k, v in track.items():
            track_mapped["info"][k] = v
        track_mapped["info"]["uri"] = f"https://www.youtube.com/watch?v={track['id']}"
        return track_mapped

    def loadtrack_generator(self, tracks: List[Mapping], search: bool = True) -> MutableMapping:
        loadtrack = {"playlistInfo": {}}
        if search:
            loadtrack["loadType"] = LoadType.SEARCH_RESULT
        else:
            loadtrack["loadType"] = LoadType.TRACK_LOADED
        loadtrack["tracks"] = [self.track_generator(t) for t in tracks]
        return loadtrack

    async def jda_get_track(self, track_id: str) -> MutableMapping:
        with contextlib.suppress(Exception):
            with contextlib.suppress(aiohttp.ContentTypeError, asyncio.TimeoutError):
                url = f"https://cache.notfab.net/track/{track_id}"
                async with self.session.request("GET", url, headers=self.headers) as r:
                    LOGGER.debug(f"JDA GET Track - Status Code: {r.status}")
                    if r.status == 200:
                        return await r.json(loads=JSON_DECODER)

        return {}

    async def jda_search_track(
        self, title: str, search_term: str, author: Optional[str]
    ) -> List[dict]:
        with contextlib.suppress(Exception):
            data = {"title": [title], "author": [author], "search": search_term}
            with contextlib.suppress(aiohttp.ContentTypeError, asyncio.TimeoutError):
                async with self.session.request(
                    "POST", POST_SEARCH_ENDPOINT, json=data, headers=self.headers
                ) as r:
                    LOGGER.debug(f"JDA POST Search - Status Code: {r.status}")
                    if r.status == 200:
                        return await r.json(loads=JSON_DECODER)
        return []

    async def jda_post_track(self, load_result: LoadResult) -> NoReturn:
        for track in load_result.tracks:
            try:
                data = {
                    "id": track._info["identifier"],
                    "title": track.title,
                    "author": track.author,
                    "length": track.length,
                    "stream": track.is_stream,
                }
                async with self.session.request(
                    "POST", POST_TRACK, json=data, headers=self.headers
                ) as r:
                    LOGGER.debug(f"JDA POST Track - Status Code: {r.status}")
                    if r.status == 200:
                        continue
            except Exception as err:
                LOGGER.exception(err)


JDACacheClient = _JDACacheClient()


def build_search_object(jda_response: List[Mapping]) -> List[Mapping]:
    output = []
    for track in jda_response:
        # Lets convert it to the expected format ...
        new_track_dict = {"track": "", "info": {}}
        new_track_dict["info"]["identifier"] = track.get("id")
        new_track_dict["info"]["isStream"] = track.get("stream", False)
        new_track_dict["info"]["isSeekable"] = not track.get("stream", False)
        new_track_dict["info"]["uri"] = (
            f"https://www.youtube.com/watch?v={new_track_dict['info']['identifier']}",
        )
        new_track_dict["info"]["position"] = 0
        new_track_dict["info"]["title"] = track.get("title")
        new_track_dict["info"]["author"] = track.get("author")
        new_track_dict["info"]["length"] = (
            sys.maxsize if track.get("stream", False) else track.get("length")
        )

        base64_string_array = bytearray((2).to_bytes(1, sys.byteorder))
        base64_string_array.extend(struct.pack(">H", len(new_track_dict["info"]["title"])))
        base64_string_array.extend(new_track_dict["info"]["title"].encode("utf-8"))

        base64_string_array.extend(struct.pack(">H", len(new_track_dict["info"]["author"])))
        base64_string_array.extend(new_track_dict["info"]["author"].encode("utf-8"))

        base64_string_array.extend(struct.pack(">q", new_track_dict["info"]["length"]))

        base64_string_array.extend(struct.pack(">H", len(new_track_dict["info"]["identifier"])))
        base64_string_array.extend(new_track_dict["info"]["identifier"].encode("utf-8"))

        base64_string_array.extend(struct.pack("?", new_track_dict["info"]["isStream"]))
        base64_string_array.extend(struct.pack("?", new_track_dict["info"]["uri"] is None))

        if new_track_dict["info"]["uri"]:
            base64_string_array.extend(struct.pack(">H", len(new_track_dict["info"]["uri"])))
        base64_string_array.extend(new_track_dict["info"]["uri"].encode("utf-8"))

        base64_string_array.extend(struct.pack(">H", len("youtube")))
        base64_string_array.extend("youtube".encode("utf-8"))

        base64_string_array.extend(struct.pack(">q", new_track_dict["info"]["position"]))
        base64_string_array.extend(struct.pack(">i", (len(base64_string_array) | 1 << 30)))

        b64_string = base64.b64encode(base64_string_array)
        new_track_dict["track"] = b64_string
        output.append(new_track_dict)

    return output
