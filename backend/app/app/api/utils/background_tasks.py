# -*- coding: utf-8 -*-
# API Imports
from app import crud
from app.api.utils import json
from app.api.utils.helper_classes import LoadResult, LoadType, Query
# from app.db.redis import QueryCacheInterface
from app.db.session import Session
from app.models.user import UserInserts
from app.utils import async_iterate
from app.db_models.user import User as DBUser
from app.db_models.query import Query as DBQuery


async def post_query_v1(db_session: Session, query: Query, llresponse: LoadResult, user: DBUser):
    valid_llresponse = json.loads(json.dumps(llresponse.raw))
    submit_response = json.loads(
        json.dumps(
            dict(
                uri=query.uri,
                request=query.lavalink_query,
                llresponse=valid_llresponse,
            )
        )
    )
    template_entry = json.loads(
        json.dumps(dict(llresponse=valid_llresponse))
    )

    objects = []
    total_entries = 0
    if llresponse.load_type in [LoadType.PLAYLIST_LOADED, LoadType.SEARCH_RESULT]:
        objects.append(DBQuery(**submit_response))

    async for track in async_iterate(llresponse.raw["tracks"]):
        new_entry = json.loads(json.dumps(template_entry))
        new_entry["title"] = track["info"]["title"]
        new_entry["author"] = track["info"]["author"]
        new_entry["llresponse"]["loadType"] = "TRACK_LOADED"
        new_entry["llresponse"]["tracks"] = [track]
        new_entry["llresponse"]["playlistInfo"] = {}
        new_entry["uri"] = track["info"]["uri"]
        new_entry["request"] = track["info"]["uri"]
        objects.append(DBQuery(**new_entry))
        total_entries += 1

    async for entry in async_iterate(objects):
        currently_in_db = crud.query.get_by_request(db_session, query=entry.request)
        if currently_in_db:
            new = crud.query.update(db_session, item=currently_in_db, item_in=entry)
        else:
            new = crud.query.create(db_session, item_in=entry)

    # cache = QueryCacheInterface(f"{query}")
    # if cache.exists():
    #     cache.set(valid_llresponse)
    user_in = UserInserts(entries_submitted=total_entries+user.entries_submitted)
    user = crud.user.update(db_session, user=user, user_in=user_in)

