from typing import Optional, List

from fastapi import Depends, HTTPException, Security
from fastapi.security import  APIKeyHeader
from sqlalchemy.orm import Session

from app import crud
from app.api.utils.db import get_db
from app.db_models.user import User
from app.models.user import UserCreate

TOKEN_HEADER = APIKeyHeader(name="Authorization", scheme_name="Server Token", auto_error=False)
HANDSHAKE_HEADER = APIKeyHeader(name="X-Token", scheme_name="User Token", auto_error=False)


def get_valid_users(
    db: Session = Depends(get_db), token: Optional[str] = Depends(TOKEN_HEADER), hand_shake: Optional[str] = Depends(HANDSHAKE_HEADER)
):
    users = []
    if token:
        user = crud.user.get_by_token(db, token=token)
        if user:
            users.append(user)

    if hand_shake:
        md5s = hand_shake.split("||")

        for md5 in md5s:
            user = crud.user.get_by_md5(db, md5=md5)
            if user:
                users.append(user)
            else:
                user_in = UserCreate(
                    is_guest=True,
                    md5=md5
                )
                user = crud.user.create(db, user_in=user_in)
                users.append(user)

    return users


def get_token_users(
    db: Session = Depends(get_db), token: Optional[str] = Depends(TOKEN_HEADER)
):
    if token:
        user = crud.user.get_by_token(db, token=token)
        if user:
            return user

    raise HTTPException(
        status_code=400, detail="The user doesn't have enough privileges"
    )


def authenticated_user(user: User = Security(get_token_users)):
    if crud.user.is_blacklisted(user):
        raise HTTPException(status_code=418, detail="I'm a tea pot")
    return user
