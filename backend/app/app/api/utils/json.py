# -*- coding: utf-8 -*-
# API Dependencies
from typing import MutableMapping, Any, Dict, Callable, Mapping

import ujson


def dumps(obj: Dict[str, Any], **kw: Mapping) -> MutableMapping:
    return ujson.dumps(obj)


def dump(obj: Dict[str, Any], fp, **kw: Mapping):
    return ujson.dump(obj, fp)


loads: Callable = ujson.loads
dumps: Callable = dumps
load: Callable = ujson.load
dump: Callable = dump
