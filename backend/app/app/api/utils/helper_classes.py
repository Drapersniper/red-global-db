# -*- coding: utf-8 -*-
# Standard Library
import re

from enum import Enum
from logging import getLogger
from typing import Optional, Union, MutableMapping
from urllib.parse import urlparse

_RE_REMOVE_START = re.compile(r"^(sc|list) ")
_RE_YOUTUBE_TIMESTAMP = re.compile(r"&t=(\d+)s?")
_RE_YOUTUBE_INDEX = re.compile(r"&index=(\d+)")
_RE_SPOTIFY_URL = re.compile(r"(http[s]?://)?(open.spotify.com)/")
_RE_SPOTIFY_TIMESTAMP = re.compile(r"#(\d+):(\d+)")
_RE_SOUNDCLOUD_TIMESTAMP = re.compile(r"#t=(\d+):(\d+)s?")
_RE_TWITCH_TIMESTAMP = re.compile(r"\?t=(\d+)h(\d+)m(\d+)s")


__all__ = ["PlaylistInfo", "LoadType", "ExceptionSeverity", "Track", "LoadResult", "Query"]

LOGGER = getLogger("AudioDB.utils.helper_classes")


class LoadType(Enum):
    """The result type of a loadtracks request."""

    TRACK_LOADED = "TRACK_LOADED"
    PLAYLIST_LOADED = "PLAYLIST_LOADED"
    SEARCH_RESULT = "SEARCH_RESULT"
    NO_MATCHES = "NO_MATCHES"
    LOAD_FAILED = "LOAD_FAILED"
    V2_COMPAT = "V2_COMPAT"


class ExceptionSeverity(Enum):
    """The result exception of a loadtracks request."""

    COMMON = "COMMON"
    SUSPICIOUS = "SUSPICIOUS"
    FATAL = "FATAL"


class PlaylistInfo:
    """The playlist data."""

    def __init__(self, name, selectedTrack) -> None:
        self.name = name
        self.selectedTrack = selectedTrack


class Track:
    """Information about a Lavalink track.

    Attributes:
    -----------
    requester : discord.User
        The user who requested the track.
    track_identifier : str
        Track identifier used by the Lavalink player to play tracks.
    seekable : bool
        Boolean determining if seeking can be done on this track.
    author : str
        The author of this track.
    length : int
        The length of this track in milliseconds.
    is_stream : bool
        Determines whether Lavalink will stream this track.
    position : int
        Current seeked position to begin playback.
    title : str
        Title of this track.
    uri : str
        The playback url of this track.
    start_timestamp: int
        The track start time in milliseconds as provided by the query.
    """

    def __init__(self, data: dict) -> None:
        self.track_identifier = data.get("track")
        self._info = data.get("info", {})
        self.seekable = self._info.get("isSeekable", False)
        self.author = self._info.get("author")
        self.length = self._info.get("length", 0)
        self.is_stream = self._info.get("isStream", False)
        self.position = self._info.get("position")
        self.title = self._info.get("title")
        self.uri = self._info.get("uri")
        self.start_timestamp = 0


class LoadResult:
    """The result of a load_tracks request.

    Attributes:
    -----------
    load_type : LoadType
        The result of the loadtracks request
    playlist_info : Optional[PlaylistInfo]
        The playlist information detected by Lavalink
    tracks : Tuple[Track, ...]
        The tracks that were loaded, if any
    """

    _fallback = {
        "loadType": LoadType.LOAD_FAILED,
        "exception": {
            "message": "Lavalink API returned an unsupported response, Please report it.",
            "severity": ExceptionSeverity.SUSPICIOUS,
        },
        "playlistInfo": {},
        "tracks": [],
    }

    def __init__(self, data: dict) -> None:
        self.raw = data
        for (key, value) in self._fallback.items():
            if key not in data:
                if (
                    key == "exception"
                    and data.get("loadType", LoadType.LOAD_FAILED) != LoadType.LOAD_FAILED
                ):
                    continue
                elif key == "exception":
                    value["message"] += (
                        f"\n{'Query: ' + data['encodedquery'] if data.get('encodedquery') else ''}"
                        f"\n{str(self.raw)}"
                    )
                self.raw.update({key: value})

        self.load_type = LoadType(self.raw["loadType"])

        is_playlist = self.raw.get("isPlaylist") or self.load_type == LoadType.PLAYLIST_LOADED
        if is_playlist is True:
            self.is_playlist = True
            self.playlist_info = PlaylistInfo(**self.raw["playlistInfo"])
        elif is_playlist is False:
            self.is_playlist = False
            self.playlist_info = None
        else:
            self.is_playlist = None
            self.playlist_info = None
        _tracks = self.raw["tracks"]
        self.tracks = tuple(Track(t) for t in _tracks)


class Query:
    """Query data class.

    Use: Query.process_input(query) to generate the Query object.
    """

    def __init__(self, query: str, **kwargs):
        query = kwargs.get("queryforced", query)
        self._raw: str = query
        self.track: str = query
        self.valid: bool = query != "InvalidQueryPlaceHolderName"
        self.is_local: bool = kwargs.get("local", False)
        self.is_spotify: bool = kwargs.get("spotify", False)
        self.is_youtube: bool = kwargs.get("youtube", False)
        self.is_soundcloud: bool = kwargs.get("soundcloud", False)
        self.is_bandcamp: bool = kwargs.get("bandcamp", False)
        self.is_vimeo: bool = kwargs.get("vimeo", False)
        self.is_mixer: bool = kwargs.get("mixer", False)
        self.is_twitch: bool = kwargs.get("twitch", False)
        self.is_other: bool = kwargs.get("other", False)
        self.is_playlist: bool = kwargs.get("playlist", False)
        self.is_album: bool = kwargs.get("album", False)
        self.is_search: bool = kwargs.get("search", False)
        self.is_stream: bool = kwargs.get("stream", False)
        self.single_track: bool = kwargs.get("single", False)
        self.id: Optional[str] = kwargs.get("id", None)
        self.invoked_from: Optional[str] = kwargs.get("invoked_from", None)
        self.local_name: Optional[str] = kwargs.get("name", None)
        self.search_subfolders: bool = kwargs.get("search_subfolders", False)
        self.spotify_uri: Optional[str] = kwargs.get("uri", None)
        self.uri: Optional[str] = kwargs.get("url", None)
        self.is_url: bool = kwargs.get("is_url", False)

        self.start_time: int = kwargs.get("start_time", 0)
        self.track_index: Optional[int] = kwargs.get("track_index", None)

        if self.invoked_from == "sc search":
            self.is_youtube = False
            self.is_soundcloud = True

        self.lavalink_query: str = self._get_query()

        if self.is_playlist or self.is_album:
            self.single_track = False

        self._hash = hash(
            (
                self.valid,
                self.is_local,
                self.is_spotify,
                self.is_youtube,
                self.is_soundcloud,
                self.is_bandcamp,
                self.is_vimeo,
                self.is_mixer,
                self.is_twitch,
                self.is_other,
                self.is_playlist,
                self.is_album,
                self.is_search,
                self.is_stream,
                self.single_track,
                self.id,
                self.spotify_uri,
                self.start_time,
                self.track_index,
                self.uri,
            )
        )

    def __str__(self):
        return str(self.lavalink_query)

    @classmethod
    def process_input(cls, query: Union[Track, "Query", str], **kwargs):
        """A replacement for :code:`lavalink.Player.load_tracks`. This will try to get a valid
        cached entry first if not found or if in valid it will then call the lavalink API.

        Parameters
        ----------
        query : Union[Query, LocalPath, lavalink.Track, str]
            The query string or LocalPath object.
        Returns
        -------
        Query
            Returns a parsed Query object.
        """
        if not query:
            query = "InvalidQueryPlaceHolderName"
        possible_values = {}

        if isinstance(query, str):
            query = query.strip("<>")

        elif isinstance(query, Query):
            for key, val in kwargs.items():
                setattr(query, key, val)
            return query
        elif isinstance(query, Track):
            possible_values["stream"] = query.is_stream
            query = query.uri

        possible_values.update(dict(**kwargs))
        possible_values.update(cls._parse(query, **kwargs))
        return cls(query, **possible_values)

    @staticmethod
    def _parse(track, **kwargs) -> MutableMapping:
        returning = {}
        track = str(track)
        if track.startswith("spotify:"):
            returning["spotify"] = True
            if ":playlist:" in track:
                returning["playlist"] = True
            elif ":album:" in track:
                returning["album"] = True
            elif ":track:" in track:
                returning["single"] = True
            _id = track.split(":", 2)[-1]
            _id = _id.split("?")[0]
            returning["id"] = _id
            if "#" in _id:
                match = re.search(_RE_SPOTIFY_TIMESTAMP, track)
                if match:
                    returning["start_time"] = (int(match.group(1)) * 60) + int(match.group(2))
            returning["uri"] = track
            return returning
        if track.startswith("sc ") or track.startswith("list "):
            if track.startswith("sc "):
                returning["invoked_from"] = "sc search"
                returning["soundcloud"] = True
            elif track.startswith("list "):
                returning["invoked_from"] = "search list"
            track = _RE_REMOVE_START.sub("", track, 1)
            returning["queryforced"] = track
        try:
            query_url = urlparse(track)
            if all([query_url.scheme, query_url.netloc, query_url.path]):
                returning["url"] = track
                returning["is_url"] = True
                url_domain = ".".join(query_url.netloc.split(".")[-2:])
                if not query_url.netloc:
                    url_domain = ".".join(query_url.path.split("/")[0].split(".")[-2:])
                if url_domain in ["youtube.com", "youtu.be"]:
                    returning["youtube"] = True
                    _has_index = "&index=" in track
                    if "&t=" in track:
                        match = re.search(_RE_YOUTUBE_TIMESTAMP, track)
                        if match:
                            returning["start_time"] = int(match.group(1))
                    if _has_index:
                        match = re.search(_RE_YOUTUBE_INDEX, track)
                        if match:
                            returning["track_index"] = int(match.group(1)) - 1
                    if all(k in track for k in ["&list=", "watch?"]):
                        returning["track_index"] = 0
                        returning["playlist"] = True
                        returning["single"] = False
                    elif all(x in track for x in ["playlist?"]):
                        returning["playlist"] = not _has_index
                        returning["single"] = _has_index
                    elif any(k in track for k in ["list="]):
                        returning["track_index"] = 0
                        returning["playlist"] = True
                        returning["single"] = False
                    else:
                        returning["single"] = True
                elif url_domain == "spotify.com":
                    returning["spotify"] = True
                    if "/playlist/" in track:
                        returning["playlist"] = True
                    elif "/album/" in track:
                        returning["album"] = True
                    elif "/track/" in track:
                        returning["single"] = True
                    val = re.sub(_RE_SPOTIFY_URL, "", track).replace("/", ":")
                    if "user:" in val:
                        val = val.split(":", 2)[-1]
                    _id = val.split(":", 1)[-1]
                    _id = _id.split("?")[0]

                    if "#" in _id:
                        _id = _id.split("#")[0]
                        match = re.search(_RE_SPOTIFY_TIMESTAMP, track)
                        if match:
                            returning["start_time"] = (int(match.group(1)) * 60) + int(
                                match.group(2)
                            )

                    returning["id"] = _id
                    returning["uri"] = f"spotify:{val}"
                elif url_domain == "soundcloud.com":
                    returning["soundcloud"] = True
                    if "#t=" in track:
                        match = re.search(_RE_SOUNDCLOUD_TIMESTAMP, track)
                        if match:
                            returning["start_time"] = (int(match.group(1)) * 60) + int(
                                match.group(2)
                            )
                    if "/sets/" in track:
                        if "?in=" in track:
                            returning["single"] = True
                        else:
                            returning["playlist"] = True
                    else:
                        returning["single"] = True
                elif url_domain == "bandcamp.com":
                    returning["bandcamp"] = True
                    if "/album/" in track:
                        returning["album"] = True
                    else:
                        returning["single"] = True
                elif url_domain == "vimeo.com":
                    returning["vimeo"] = True
                elif url_domain in ["mixer.com", "beam.pro"]:
                    returning["mixer"] = True
                elif url_domain == "twitch.tv":
                    returning["twitch"] = True
                    if "?t=" in track:
                        match = re.search(_RE_TWITCH_TIMESTAMP, track)
                        if match:
                            returning["start_time"] = (
                                (int(match.group(1)) * 60 * 60)
                                + (int(match.group(2)) * 60)
                                + int(match.group(3))
                            )

                    if not any(x in track for x in ["/clip/", "/videos/"]):
                        returning["stream"] = True
                else:
                    returning["other"] = True
                    returning["single"] = True
            else:
                if kwargs.get("soundcloud", False):
                    returning["soundcloud"] = True
                else:
                    returning["youtube"] = True
                returning["search"] = True
                returning["single"] = True
        except Exception:
            returning["search"] = True
            returning["youtube"] = True
            returning["single"] = True
        return returning

    def _get_query(self):
        if self.is_spotify:
            return self.spotify_uri
        elif self.is_search and self.is_youtube:
            return f"ytsearch:{self.track}"
        elif self.is_search and self.is_soundcloud:
            return f"scsearch:{self.track}"
        return self.track

    def to_string_user(self):
        return str(self._raw)
